package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"golang.org/x/time/rate"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	PhotosDir  = "photos"
	PhotosPath = "/" + PhotosDir + "/"
	Port       = "4300"
)

type AlbumAddRequest struct {
	AlbumId        string      `json:"albumId"`
	Photos         []Photo     `json:"photos"`
	MaxPhotoAmount json.Number `json:"maxPhotoAmount"`
}

type Photo struct {
	Url      string `json:"url"`
	Filename string `json:"filename"`
}

type AlbumAddResponse struct {
	Success bool `json:"success"`
}

type PhotoUrlsGetRequest struct {
	AlbumId string `json:"albumId"`
}

type PhotoUrlsGetResponse struct {
	Success        bool     `json:"success"`
	PhotoUrls      []string `json:"photoUrls"`
	Selected       []bool   `json:"selected"`
	MaxPhotoAmount int      `json:"maxPhotoAmount"`
}

const albumsFilePath string = "albums.json"

var albums []Album
var albumsMu sync.RWMutex
var albumsFileMu sync.RWMutex

type Album struct {
	AlbumId        string   `json:"albumId"`
	MaxPhotoAmount int      `json:"maxPhotoAmount"`
	FileNames      []string `json:"fileNames"`
	Ratings        []Rating `json:"ratings"`
}

type Rating struct {
	Ip        string   `json:"ip"`
	FileNames []string `json:"filenames"`
}

type PhotoSelectRequest struct {
	AlbumId  string `json:"albumId"`
	Filename string `json:"filename"`
}

type PhotoSelectResponse struct {
	Success bool `json:"success"`
}

func main() {
	LoadAlbumsJson()

	if _, err := os.Stat(PhotosDir); os.IsNotExist(err) {
		if err := os.Mkdir(PhotosDir, os.ModePerm); err != nil {
			panic(err)
		}
	}

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/add", AddAlbum).Methods("POST")

	router.HandleFunc("/get", GetPhotoUrls).Methods("POST")

	// Only for development purposes (ignore in production)
	router.PathPrefix(PhotosPath).Handler(http.StripPrefix(PhotosPath, http.FileServer(http.Dir("."+PhotosPath))))

	router.HandleFunc("/select", func(w http.ResponseWriter, r *http.Request) {
		SelectPhoto(w, r, true)
	}).Methods("POST")
	router.HandleFunc("/deselect", func(w http.ResponseWriter, r *http.Request) {
		SelectPhoto(w, r, false)
	}).Methods("POST")

	log.Fatal(http.ListenAndServe(":"+Port, Limit(router)))
}

var visitors = make(map[string]*rate.Limiter)
var visitorsMu sync.Mutex

func GetVisitor(ip string) *rate.Limiter {
	visitorsMu.Lock()
	defer visitorsMu.Unlock()
	limiter, exists := visitors[ip]
	if !exists {
		limiter = rate.NewLimiter(rate.Every(time.Second/3), 2) // allows 3 requests per second
		visitors[ip] = limiter
	}
	return limiter
}

func GetIp(r *http.Request) string {
	ip := r.Header.Get("X-Real-Ip")
	if ip == "" {
		ip = r.Header.Get("X-Forwarded-For")
	}
	if ip == "" {
		ip = r.RemoteAddr
	}
	return ip
}

func Limit(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !strings.HasPrefix(r.URL.Path, PhotosPath) {
			ip := GetIp(r)
			if !GetVisitor(ip).Allow() {
				http.Error(w, http.StatusText(429), http.StatusTooManyRequests)
				return
			}
		}

		next.ServeHTTP(w, r)
	})
}

func LoadAlbumsJson() {
	if _, err := os.Stat(albumsFilePath); os.IsNotExist(err) {
		var emptyAlbums []Album
		if err := json.Unmarshal([]byte(`[]`), &emptyAlbums); err != nil {
			panic(err)
		}
		emptyAlbumsJson, _ := json.Marshal(emptyAlbums)
		albumsFileMu.Lock()
		defer albumsFileMu.Unlock()
		if err := ioutil.WriteFile(albumsFilePath, emptyAlbumsJson, 0644); err != nil {
			panic(err)
		}
	}
	albumsFileMu.RLock()
	defer albumsFileMu.RUnlock()
	jsonFile, err := os.Open(albumsFilePath)
	if err != nil {
		panic(err)
	}

	albumsJson, _ := ioutil.ReadAll(jsonFile)

	albumsMu.Lock()
	defer albumsMu.Unlock()
	if err := json.Unmarshal(albumsJson, &albums); err != nil {
		panic(err)
	}

	if err := jsonFile.Close(); err != nil {
		panic(err)
	}
}

func StoreAlbumsJson() {
	albumsMu.RLock()
	albumsJson, _ := json.MarshalIndent(albums, "", "  ")
	albumsMu.RUnlock()
	albumsFileMu.Lock()
	defer albumsFileMu.Unlock()
	if err := ioutil.WriteFile(albumsFilePath, albumsJson, 0644); err != nil {
		panic(err)
	}
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func setHeaders(w *http.ResponseWriter) {
	(*w).Header().Set("Content-Type", "application/json; charset=UTF-8")
	(*w).WriteHeader(http.StatusOK)
}

func AddAlbum(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var t AlbumAddRequest
	if err := decoder.Decode(&t); err != nil {
		panic(err)
	}

	directoryPath := PhotosDir + "/" + t.AlbumId
	if err := os.Mkdir(directoryPath, os.ModePerm); err != nil {
		panic(err)
	}
	fileNames := make([]string, len(t.Photos))
	for i, photo := range t.Photos {
		DownloadFile(photo.Url, directoryPath, photo.Filename)
		fileNames[i] = photo.Filename
	}

	maxPhotoAmount, err := t.MaxPhotoAmount.Int64()
	if err != nil {
		panic(err)
	}

	albumsMu.Lock()
	albums = append(albums, Album{
		AlbumId:        t.AlbumId,
		FileNames:      fileNames,
		Ratings:        []Rating{},
		MaxPhotoAmount: int(maxPhotoAmount),
	})
	albumsMu.Unlock()

	StoreAlbumsJson()

	enableCors(&w)
	setHeaders(&w)

	response := AlbumAddResponse{
		Success: true,
	}
	if err := json.NewEncoder(w).Encode(response); err != nil {
		panic(err)
	}
}

func DownloadFile(url string, directoryName string, fileName string) {
	response, err := http.Get(url)
	if err != nil {
		panic(err)
	}

	file, err2 := os.Create(directoryName + "/" + fileName)
	if err2 != nil {
		panic(err2)
	}

	_, err3 := io.Copy(file, response.Body)
	if err3 != nil {
		panic(err3)
	}

	if err := response.Body.Close(); err != nil {
		panic(err)
	}
	if err := file.Close(); err != nil {
		panic(err)
	}
}

func GetAlbum(albumId string) (*Album, bool) {
	albumsMu.RLock()
	defer albumsMu.RUnlock()
	for i, album := range albums {
		if album.AlbumId == albumId {
			return &albums[i], true // return pointer to album
		}
	}
	return nil, false
}

func GetPhotoUrls(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var t PhotoUrlsGetRequest
	if err := decoder.Decode(&t); err != nil {
		panic(err)
	}

	album, albumIdValid := GetAlbum(t.AlbumId)
	var photoUrls []string
	var selected []bool
	maxPhotoAmount := 0
	if albumIdValid {
		ip := GetIp(r)
		rating, hasRated := HasRatedAlbum(album.Ratings, ip)
		photoUrls = make([]string, len(album.FileNames))
		selected = make([]bool, len(album.FileNames))
		for i, filename := range album.FileNames {
			photoUrls[i] = "/" + t.AlbumId + "/" + filename
			selected[i] = hasRated && Contains(rating.FileNames, filename)
		}
		maxPhotoAmount = album.MaxPhotoAmount
	}

	enableCors(&w)
	setHeaders(&w)

	response := PhotoUrlsGetResponse{
		Success:        albumIdValid,
		PhotoUrls:      photoUrls,
		Selected:       selected,
		MaxPhotoAmount: maxPhotoAmount,
	}
	if err := json.NewEncoder(w).Encode(response); err != nil {
		panic(err)
	}
}

func Contains(arr []string, elem string) bool {
	for _, str := range arr {
		if str == elem {
			return true
		}
	}
	return false
}

func RemoveFrom(arr []string, elem string) []string {
	newArr := make([]string, len(arr)-1)
	removed := false
	for i, arrElem := range arr {
		if arrElem == elem {
			removed = true
			continue
		}
		if removed {
			newArr[i-1] = arrElem
		} else {
			newArr[i] = arrElem
		}
	}
	return newArr
}

func RemoveFromRatings(ratings []Rating, rating *Rating) []Rating {
	newRatings := make([]Rating, len(ratings)-1)
	removed := false
	for i := range ratings {
		arrRating := &ratings[i]
		if arrRating == rating {
			removed = true
			continue
		}
		if removed {
			newRatings[i-1] = *arrRating
		} else {
			newRatings[i] = *arrRating
		}
	}
	return newRatings
}

func HasRatedAlbum(ratings []Rating, ip string) (*Rating, bool) {
	for i, rating := range ratings {
		if rating.Ip == ip {
			return &ratings[i], true // return pointer to rating
		}
	}
	return nil, false
}

func SelectPhoto(w http.ResponseWriter, r *http.Request, select_ bool) {
	decoder := json.NewDecoder(r.Body)
	var t PhotoSelectRequest
	if err := decoder.Decode(&t); err != nil {
		panic(err)
	}

	album, success := GetAlbum(t.AlbumId)
	ip := GetIp(r)
	fileValid := Contains(album.FileNames, t.Filename)
	if success && ip != "" && fileValid {
		if select_ {
			success = AddRating(album, t.Filename, ip)
		} else {
			success = RemoveRating(album, t.Filename, ip)
		}
	}

	enableCors(&w)
	setHeaders(&w)

	response := PhotoSelectResponse{
		Success: success,
	}
	if err := json.NewEncoder(w).Encode(response); err != nil {
		panic(err)
	}
}

func AddRating(album *Album, filename string, ip string) bool {
	rating, hasRated := HasRatedAlbum(album.Ratings, ip)
	if hasRated {
		if Contains(rating.FileNames, filename) {
			return false
		}
		if len(rating.FileNames) >= album.MaxPhotoAmount {
			return false
		}
		rating.FileNames = append(rating.FileNames, filename)
		StoreAlbumsJson()
		return true
	} else {
		album.Ratings = append(album.Ratings, Rating{
			Ip:        ip,
			FileNames: []string{filename},
		})
		StoreAlbumsJson()
		return true
	}
}

func RemoveRating(album *Album, filename string, ip string) bool {
	rating, hasRated := HasRatedAlbum(album.Ratings, ip)
	if hasRated {
		if Contains(rating.FileNames, filename) {
			if len(rating.FileNames) <= 1 {
				album.Ratings = RemoveFromRatings(album.Ratings, rating)
			} else {
				rating.FileNames = RemoveFrom(rating.FileNames, filename)
			}
			StoreAlbumsJson()
			return true
		}
		return false
	} else {
		return false
	}
}
